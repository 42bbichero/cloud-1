/**
 *  Need to connect to online api
 *  Firts oauth with your client_id
 *  Then get the code from last request
 *  Finaly request api endpoint to get token key for each requests
 */

// Component dependencies
const async = require("async");
const colors = require('colors');

// File dependencies
const server = require("./api/lib/server");

// Stores the server ID
const serverId = '';

// Stores the server informations
const info = {
  "action": "reboot",
};

// Set a boolean to display or not comments
const DEBUG = true;


async.waterfall([
  /**
   * Function that list servers
   */
  function(nextFunction) {
    server.listServers((response) => {
      if (DEBUG) {
        console.log('LIST SERVERS'.rainbow);
      }

      if (response.err) {
        return nextFunction(response.err);
      }

      if (DEBUG) {
        console.log(response.res);
      }

      return nextFunction();
    });
  },

  /**
   * Function that list available server actions
   * @param serverId
   */
  function(nextFunction) {
    server.listServerActions(serverId, (response) => {
      if (DEBUG) {
        console.log('LIST SERVER ACTIONS'.rainbow);
      }

      if (response.err) {
        return nextFunction(response.err);
      }

      if (DEBUG) {
        console.log(response.res);
      }

      return nextFunction();
    });
  },

  /**
   * Function that execute an action on a server
   * @param serverId
   * @param action
   */
  function(nextFunction) {
    server.executeServerAction(serverId, info, (response) => {
      if (DEBUG) {
        console.log('EXECUTE SERVER ACTION'.rainbow);
      }

      if (response.err) {
        return nextFunction(response.err);
      }

      if (DEBUG) {
        console.log(response.res);
      }

      return nextFunction();
    });
  },


  /**
   * Function that removes a specified server
   * @param serverId
   */
  // function(nextFunction) {
  //   server.deleteServer(serverId, (response) => {
  //     if (DEBUG) {
  //       console.log('DELETE SERVER'.rainbow);
  //     };
  //
  //     if (response.err) {
  //       return nextFunction(response.err);
  //     };
  //
  //     if (DEBUG) {
  //       console.log(response);
  //     };
  //
  //     return nextFunction();
  //   });
  // },

], function(err) {
  if (err) {
    console.log(err);
  }
});
