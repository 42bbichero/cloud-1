# cloud-1

### usefull links :
Scaleway API doc => https://developer.scaleway.com/#header-request-and-response
Loadbalancing wordPress => https://blog.rackspace.com/configuring-a-load-balanced-wordpress-website-with-the-rackspace-cloud

## Needed scaleway routes
src api url => https://cp-par1.scaleway.com 

# server
POST : /servers => 201
GET : /servers => 200

GET : /servers/{server_id} => 200
PUT : /servers/{server_id} => 200
DELETE : /servers/{server_id} => 204

GET : /servers/{server_id}/action => 200
POST : /servers/{server_id}/action => 202
