// Component dependencies
const colors = require('colors');

// File dependencies
const config = require("../../config.json");
const fct = require("../utils");


module.exports = {
  /**
   *  GET: Get user info, used to valid apiKey
   */
  listServers: async (callback) => {
    let response = {};

    try {
      response.res = await fct.GET(`${config.url}/servers`);
    } catch (err) {
      response.err = err;
    }

    return callback(response);
  },

  /**
   *  GET: List all actions available on a server
   */
  listServerActions: async (serverId, callback) => {
    if (!serverId) {
      console.log('No Server ID.')
      return;
    }

    let response = {};

    try {
      response.res = await fct.GET(`${config.url}/servers/${serverId}/action`);
    } catch (err) {
      response.err = err;
    }

    return callback(response);
  },

  /**
   *  GET: List all actions available on a server
   */
  getServer: async (serverId, callback) => {
    if (!serverId) {
      console.log('No Server ID.')
      return;
    }

    let response = {};

    try {
      response.res = await fct.GET(`${config.url}/servers/${serverId}`);
    } catch (err) {
      response.err = err;
    }

    return callback(response);
  },

  /**
   *  POST: List all actions available on a server
   */
  executeServerAction: async (serverId, info, callback) => {
    if (!serverId) {
      console.log('No Server ID.')
      return;
    }

    let response = {};

    try {
      response.res = await fct.POST(`${config.url}/servers/${serverId}/action`, info);
    } catch (err) {
      response.err = err;
    }

    return callback(response);
  },

  /**
   *  DELETE: Remove the specified server
   */
  deleteServer: async (serverId, callback) => {
    if (!serverId) {
      console.log('No Server ID.')
      return;
    }

    let response = {};

    try {
      response.res = await fct.DELETE(`${config.url}/servers/${serverId}`);
    } catch (err) {
      response.err = err;
    }

    return callback(response);
  },

};
