// Component dependencies
const request = require("request-promise-native");
const colors = require('colors');

// File dependencies
const config = require("../config.json");

// Stores the header configuration
const headers = {
  "X-Auth-Token": config.key.private,
  "Content-Type": "application/json"
};

// Set a boolean to display or not comments
const DEBUG = false;

module.exports = {
  // Function that holds the POST query
  POST: async (url, form) => {
    // Set the POST options
    const options = {
      url: url,
      body: form,
      json: true,
      headers: headers,
    };

    let result = false;

    // Send request and try it
    try {
      result = await request.post(options);
    } catch (err) {

      if (DEBUG) {
        console.log('POST ERROR'.underline.red);
      }

      return err;
    }
    if (DEBUG) {
      console.log('POST SUCCESS'.underline.green);
    }

    return result;
  },

  // Function that holds the GET query
  GET: async (url) => {
    // Set the GET options
    const options = {
      url: url,
      headers: headers,
    };

    let result = false;

    // Send request and try it
    try {
      result = await request.get(options);
    } catch (err) {

      if (DEBUG) {
        console.log('GET ERROR'.underline.red);
      }

      return err;
    }

    if (DEBUG) {
      console.log('GET SUCCESS'.underline.green);
    }

    return result;
  },

  // Function that holds the DELETE query
  DELETE: async (url) => {
    // Set the DELETE options
    const options = {
      url: url,
      headers: headers,
    };

    // Send request and try it
    try {
      result = await request.del(options);
    } catch (err) {

      if (DEBUG) {
        console.log('DELETE ERROR'.underline.red);
      }

      return err;
    }

    if (DEBUG) {
      console.log('DELETE SUCCESS'.underline.green);
    }

    return result;
  },

  // Function that holds the PATCH query
  PATCH: async (url, form) => {
    // Set the PATCH options
    const options = {
      url: url,
      form: form,
      headers: headers,
    };

    // Send request and try it
    try {
      result = await request.patch(options);
    } catch (err) {

      if (DEBUG) {
        console.log('PATCH ERROR'.underline.red);
      }

      return err;
    }

    if (DEBUG) {
      console.log('PATCH SUCCESS'.underline.green);
    }

    return result;
  }
};
